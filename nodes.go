package nodes

import (
	"encoding/gob"
	"net"
	"time"
)

type Node struct {
	blockList []blocks.Block
}

//Maintain a connection to the master node to listen and send a block when wanted and get a block when received
func MaintainConnection() {

	var newBlk blocks.Block

	//Create a connection for getting new blocks
	manConLn, lnErr := net.Listen("tcp", ":9045")
	if lnErr != nil {
		time.Sleep(3 * time.Second)
		manConLn.Close()
		go MaintainConnection()
		return
	}

	for {
		manCon, accErr := manConLn.Accept()
		if accErr != nil {
			time.Sleep(3 * time.Second)
			manCon.Close()
			go MaintainConnection()
			return
		}

		dec := gob.NewDecoder(manCon)
		dec.Decode(&newBlk)

	}
}

//Maintain a file that holds the blocks that exist on this node
func MaintainFile() {

}
